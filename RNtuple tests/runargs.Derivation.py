#!/usr/bin/env athena.py
# Run arguments file auto-generated on Wed May 15 18:45:41 2024 by:
# JobTransform: Derivation
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'Derivation'

runArgs.perfmon = 'fullmonmt'
runArgs.maxEvents = 20666
runArgs.formats = ['PHYS']
runArgs.preExec = ['flags.Output.StorageTechnology.EventData="ROOTRNTUPLE";']

 # Input data
runArgs.inputAODFile = ['/data/tovsiann/lrgtest/data23_13p6TeV/data23_13p6TeV.00453713.physics_Main.merge.AOD.f1357_m2176._lb1481._0002.1']
runArgs.inputAODFileType = 'AOD'
runArgs.inputAODFileNentries = 20666
runArgs.AODFileIO = 'input'

 # Output data
runArgs.outputDAOD_PHYSFile = 'DAOD_PHYS.pool.root'
runArgs.outputDAOD_PHYSFileType = 'AOD'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets

 # AthenaMP Options. nprocs = 8
runArgs.athenaMPWorkerTopDir = 'athenaMP-workers-Derivation-DerivationFramework'
runArgs.athenaMPOutputReportFile = 'athenaMP-outputs-Derivation-DerivationFramework'
runArgs.athenaMPEventOrdersFile = 'athenamp_eventorders.txt.Derivation'
runArgs.athenaMPCollectSubprocessLogs = True
runArgs.athenaMPStrategy = 'SharedQueue'
runArgs.sharedWriter = True
runArgs.parallelCompression = False

 # Executor flags
runArgs.totalExecutorSteps = 0

 # Threading flags
runArgs.nprocs = 8
runArgs.threads = 0
runArgs.concurrentEvents = 0
 # Import skeleton and execute it
from DerivationFrameworkConfiguration.DerivationSkeleton import fromRunArgs
fromRunArgs(runArgs)