#!/bin/bash
export ATHENA_CORE_NUMBER=8
Reco_tf.py \
  --CA 'default:True' 'RDOtoRDOTrigger:False' \
  --inputHITSFile=/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/mc21_13p6TeV/mc21_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.merge.HITS.e8453_e8455_s3873_s3874/HITS.29625936._002926.pool.root.1 \
  --inputRDO_BKGFile=mc21a_presampling_1000_200.RDO.pool.root \
  --outputAODFile=myAOD_200.pool.root \
  --perfmon 'fullmonmt' \
  --maxEvents=1000 \
  --multithreaded=True \
  --preInclude 'all:Campaigns/MC21a.py'\
  --postInclude 'default:PyJobTransforms/UseFrontier.py' \
  --skipEvents=0 \
  --autoConfiguration=everything \
  --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' \
  --geometryVersion=default:ATLAS-R3S-2021-03-00-00 \
  --runNumber=601237 \
  --digiSeedOffset1=232 \
  --digiSeedOffset2=232 \
  --AMITag=r13768 \
  --steering 'doOverlay' 'doRDO_TRIG' 'doTRIGtoALL'
~                                                   
