#!/bin/bash

Events=1000
DigiOutFileName="mc21a_presampling_1000_200.RDO.pool.root"
HSHitsFile="myHITS.pool.root"
HighPtMinbiasHitsFiles="mc21_13p6TeV.800831.Py8EG_minbias_inelastic_highjetphotonlepton.merge.HITS.e8453_e8455_s3876_s3880/*"
LowPtMinbiasHitsFiles="mc21_13p6TeV.900311.Epos_minbias_inelastic_lowjetphoton.merge.HITS.e8453_s3876_s3880/*"


# full run
Digi_tf.py \
--PileUpPresampling True \
--conditionsTag default:OFLCOND-MC21-SDR-RUN3-07 \
--digiSeedOffset1 170 --digiSeedOffset2 170 \
--digiSteeringConf 'StandardSignalOnlyTruth' \
--geometryVersion default:ATLAS-R3S-2021-03-00-00 \
--inputHITSFile ${HSHitsFile} \
--inputHighPtMinbiasHitsFile ${HighPtMinbiasHitsFiles} \
--inputLowPtMinbiasHitsFile ${LowPtMinbiasHitsFiles} \
--jobNumber 1 \
--maxEvents ${Events} \
--outputRDOFile ${DigiOutFileName} \
--postInclude 'default:PyJobTransforms/UseFrontier.py' \
--preInclude 'HITtoRDO:Campaigns/PileUpPresamplingMC21aSingleBeamspot.py' \
--preExec 'HITtoRDO:userRunLumiOverride={"run":410000, "startmu":200.0, "endmu":201.0, "stepmu":1.0, "startlb":1, "timestamp": 1650000000};' \
--skipEvents 0
