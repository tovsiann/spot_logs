#!/bin/bash
export ATHENA_CORE_NUMBER=8
Sim_tf.py  --multithreaded 'True' \
 --preInclude 'EVNTtoHITS:Campaigns.MC21SimulationSingleIoV' \
 --inputEVNTFile "mc21_13p6TeV.900149.PG_single_nu_Pt50.merge.EVNT.e8442_e8447_tid28698131_00/*" \
 --maxEvents -1 \
 --AMIConfig 's3864' \
 --outputHITSFile 'myHITS.pool.root' \
 --conditionsTag 'OFLCOND-MC21-SDR-RUN3-07' \
 --geometryVersion 'ATLAS-R3S-2021-03-00-00' >& __log.txt;
